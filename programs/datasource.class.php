<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
bab_functionality::includeOriginal('Ovml');

/**
 * Convert the xml file to datasource
 *
 */
class Func_Ovml_Datasource extends Func_Ovml 
{
	/**
	 * @var DOMDocument
	 */
	private $xml;
	
	/**
	 * 
	 * @var babOvTemplate
	 */
	private $template;
	
	
	public function getDescription()
	{
		return 'OVML datasource, with a XML description file, create an xml DOMDocument datasource containing OVML data';
	}
	
	
	/**
	 * Set datasource description XML file
	 * @param string $filename
	 */
	public function setDatasourceFile($filename)
	{
		$this->xml = DOMDocument::load($filename);
		$this->template = new babOvTemplate;
	}
	
	/**
	 * @return string | null
	 */
	protected function attribute($node, $name)
	{
		$atNode = $node->attributes->getNamedItem($name);
		if (!isset($atNode))
		{
			return null;
		}
		return $atNode->nodeValue;
	}
	
	/**
	 * Load containers from OVML
	 * @param	
	 */
	protected function loadNodeContainers(DomNode $node)
	{
		$arr = array();
		
		foreach($node->childNodes as $containerNode)
		{
			/*@var $containerNode DomNode  */
			if (XML_ELEMENT_NODE === $containerNode->nodeType && 'container' === $containerNode->nodeName)
			{
				$name = $this->attribute($containerNode,'name');
				$container = bab_functionality::get('Ovml/Container/'.$name, false);
				/*@var $container Func_Ovml_Container */
				
				
				$ctx = new bab_context($name);
				$ctx->setContent('');
				$this->template->push_ctx($ctx);
				
				$attributes = $this->getAttributes($containerNode);
				
				foreach($attributes as $key => $val )
				{
					$this->template->curctx->push($key, $val);
				}
				
				$container->setOvmlContext($this->template);
				
				$arr[$name] = array(
					'attributes' => $attributes,
					'itemname' => 'ContainerItem',
					'items' => $this->processContainer($containerNode, $container)
				);
			}
		}
		
		return $arr;
	}
	
	/**
	 * Get attributes from a container dom node
	 * @return array
	 */
	protected function getAttributes(DomNode $node)
	{
		$attributes = array();
		
		foreach ($node->childNodes as $attributeNode)
		{
			if (XML_ELEMENT_NODE === $attributeNode->nodeType && 'attribute' === $attributeNode->nodeName)
			{
				/*@var  $attributeNode DomNode */
				
				$name 	= $this->attribute($attributeNode	, 'name');
				$value 	= $attributeNode->textContent;
				$get 	= $this->attribute($attributeNode	, 'get');
				$context= $this->attribute($attributeNode	, 'context');
				
				if (isset($context))
				{
					$value = $this->template->get_value($context);
				}

				if (isset($get))
				{
					if (!isset($_GET[$get]))
					{
						throw new Exception('missing url parameter '.$get);
					}
					
					$value = bab_gp($get);
				}
				
				$attributes[$name] = $value;
			}
		}
		
		return $attributes;
	}
	
	
	/**
	 * 
	 * @param DomNode $node
	 * @return array
	 */
	protected function getVariables(DomNode $node)
	{
		$variables = array();
		
		foreach ($node->childNodes as $variableNode)
		{
			if (XML_ELEMENT_NODE === $variableNode->nodeType && 'variable' === $variableNode->nodeName)
			{
		
		
				$name = $this->attribute($variableNode,'name');
				$variables[$name] = $this->getAttributes($variableNode);
			}
			
			
		}
		
		return $variables;
	}
	
	
	/**
	 * Process container to datasource
	 * call sub containers
	 */
	protected function processContainer(DomNode $node, Func_Ovml_Container $container)
	{
		$variables = $this->getVariables($node);
		$arr = array();
		
		while ($container->getnext())
		{
			$cResult = array();
			
			foreach($variables as $name => $attributes)
			{
				$value = $this->template->format_output(
					$this->template->curctx->get($name), 
					$attributes, 
					$this->template->curctx->getFormat($name)
				);

				$cResult[$name] = bab_convertStringFromDatabase($value, 'UTF-8');
			}
			
			$cResult += $this->loadNodeContainers($node);
			
			$arr[] = $cResult;
		}
		
		return $arr;
	}
	

	
	
	
	private function array_to_xml($itemname, $items, DOMNode $node, $document) {
		
		foreach($items as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $document->createElement($key);
				}
				else{
					$subnode = $document->createElement($itemname);
				}
				
				$node->appendChild($subnode);
				if (isset($value['itemname']))
				{
					foreach($value['attributes'] as $key => $val)
					{
						$subnode->setAttribute($key, $val);
					}
					
					$this->array_to_xml($value['itemname'], $value['items'], $subnode, $document);
				} else {
					$this->array_to_xml('unknown', $value, $subnode, $document);
				}
			}
			else {
				$subnode = $document->createElement($key);
				if (!empty($value))
				{
					$subnode->appendChild(new DOMText($value));
				}
				$node->appendChild($subnode);
			}
		}
	}
	
	
	/**
	 * 
	 * @return array
	 */
	public function getArray()
	{
		return $this->loadNodeContainers($this->xml->firstChild);
	}
	
	
	/**
	 * @return  DomDocument
	 */
	public function getDomDocument() {
		
		$document = new DOMDocument('1.0', 'UTF-8');
		$rootnode = $document->createElement('datasource');
		$document->appendChild($rootnode);
		
		try {
			
			$arr = $this->getArray();
			$this->array_to_xml('datasource', $arr, $rootnode, $document);
		} catch(Exception $e)
		{
			$rootnode->appendChild($document->createElement(get_class($e), $e->getMessage()));
		}
		
		return $document;
	}
}